﻿using NUnit.Framework;
using refactoring_with_regex;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public class TestExtractStoryData
    {

        /* 
         * Your mission:
         * 1) REFACTORING: Make it simple to add a new extraction.
         * 2) Add a case for: 'Progresja', at H2 header and blocked by H1 or H2 headers.
         * 
         * ## Progresja
         * text
         * ## Blocker
         * 
         * 3) Add a case for: 'Time', at H1 header. It has 2 H2 headers: 'Opóźnienie' and 'Czas', which take integers
         *    and return a struct of delay, time. Blocked by a H1 header.
         *    
         * # Time
         * ## Opóźnienie
         * delay: 3
         * ## Czas
         * time: 5
         * # Blocker
         * 
         * 4) Of course, add tests for it.
         */

        [Test]
        public void TestExtractLocationData_Invalid()
        {
            // Given
            string someSections = "" +
                "# Different Header:\n" +
                "Some data\n" +
                "# Different Header\n";

            // Expected

            string locationTextExpected = "default";

            // When
            var esd = new ExtractStoryDataFromText();
            string locationTextActual = esd.ExtractLocationSection(someSections);

            // Then
            Assert.AreEqual(locationTextExpected.Trim(), locationTextActual);
        }

        [Test]
        public void TestExtractLocationData_Proper_HeaderH2Text()
        {
            // Given
            string withLocationSection = "" +
                "# Lokalizacje:\n" +
                "## Location subheader\n" +
                "Text\n" +
                "# Different Header\n";

            // Expected

            string locationTextExpected = "## Location subheader\nText";

            // When
            var esd = new ExtractStoryDataFromText();
            string locationTextActual = esd.ExtractLocationSection(withLocationSection);

            // Then
            Assert.AreEqual(locationTextExpected.Trim(), locationTextActual);
        }

        [Test]
        public void TestExtractLocationData_Proper_NormalText()
        {
            // Given
            string withLocationSection = "" +
                "# Lokalizacje:\n" +
                "Location data\n" +
                "# Different Header\n";

            // Expected

            string locationTextExpected = "Location data";

            // When
            var esd = new ExtractStoryDataFromText();
            string locationTextActual = esd.ExtractLocationSection(withLocationSection);

            // Then
            Assert.AreEqual(locationTextExpected.Trim(), locationTextActual);
        }

        [Test]
        public void TestExtractSummaryData_Proper_HeaderH2Text()
        {
            // Given
            string withProperSection = "" +
                "# streszczenie:\n" +
                "summary data\n" +
                "## Different subheader\n" +
                "Text\n" +
                "# Different Header\n";

            // Expected

            string locationTextExpected = "summary data";

            // When
            var esd = new ExtractStoryDataFromText();
            string locationTextActual = esd.ExtractSummarySection(withProperSection);

            // Then
            Assert.AreEqual(locationTextExpected.Trim(), locationTextActual);
        }

        [Test]
        public void TestExtractSummaryData_Proper_NormalText()
        {
            // Given
            string withProperSection = "" +
                "# Streszczenie:\n" +
                "Summary data\n" +
                "# Different Header\n";

            // Expected

            string locationTextExpected = "Summary data";

            // When
            var esd = new ExtractStoryDataFromText();
            string locationTextActual = esd.ExtractSummarySection(withProperSection);

            // Then
            Assert.AreEqual(locationTextExpected.Trim(), locationTextActual);
        }

        [Test]
        public void TestExtractCampaignData_Proper_NormalText()
        {
            // Given
            string withProperSection = "" +
                "## Kontynuacja:\n" +
                "Random text\n" +
                "### Chronologiczna\n" +
                "Random text\n" +
                "### Kampanijna\n" +
                "* [Link label](www.link-anchor.com)\n" +
                "### Logiczna\n" +
                "Random text" +
                "# Different H1 Header\n";

            // Expected

            string textExpected = "www.link-anchor.com";

            // When
            var esd = new ExtractStoryDataFromText();
            string textActual = esd.ExtractCampaignContinuitySection(withProperSection);

            // Then
            Assert.AreEqual(textExpected.Trim(), textActual);
        }

    }
}