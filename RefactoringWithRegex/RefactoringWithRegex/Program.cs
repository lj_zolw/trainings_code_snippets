﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace refactoring_with_regex
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = File.ReadAllText(@"C:\fake-nonexistent-file.txt");

            ExtractStoryDataFromText extractor = new ExtractStoryDataFromText();

            string summary = extractor.ExtractSummarySection(text);
            string location = extractor.ExtractLocationSection(text);
            string continuity = extractor.ExtractCampaignContinuitySection(text);
            string progresja = extractor.ExtractProgressionSection(text);
            (string delay, string time) = extractor.ExtractTimeSection(text);

            Console.Out.Write("I have read: " + summary + location + continuity + progresja + delay + time);
            
        }
    }
}
