﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace refactoring_with_regex
{
    public class ExtractStoryDataFromText
    {
        /* Original Python code under refactoring is here:
         https://gitlab.com/riggeddice/rdbutler/blob/5a233f161f2b8ac6ca2bc6025c0964dcbc4c0b56/src/application/entities/stories/extract.py
         Feel free to look at it to understand the full case; we use a simplified one here
         /**/

        public (string delay, string time) ExtractTimeSection(string text)
        {
            throw new NotImplementedException();
        }

        public string ExtractProgressionSection(string text)
        {
            throw new NotImplementedException();
        }


        public string ExtractLocationSection(string storyText)
        {
            // To understand, go to the link below:
            // https://regexper.com/#^%23\s[Ll]okalizacje[%3A|\s]*%24%28.%2B%3F%29%28\Z|^%23\s\w%2B%29
            // This is a regex visualizer
            string extractingRegex = @"^#\s[Ll]okalizacje[:|\s]*$(.+?)(\Z|^#\s\w+)";

            // To play with regex, go to the link below:
            // https://regex101.com/
            // This is a decent regex editor

            string result = null;
            try
            {
                MatchCollection matches = Regex.Matches(storyText, extractingRegex, RegexOptions.Singleline | RegexOptions.Multiline);
                result = matches[0].Groups[1].Value;
            }
            catch (ArgumentException)
            {
            }

            if (result == null)
            {
                return "default";
            }

            string body = result.Trim();
            return body;
        }

        public string ExtractSummarySection(string storyText)
        {
            string extractingRegex = @"^#\s[Ss]treszczenie[:|\s]*$(.+?)(\Z|^#{1,2}\s\w+)";

            string result = null;
            try
            {
                MatchCollection matches = Regex.Matches(storyText, extractingRegex, RegexOptions.Singleline | RegexOptions.Multiline);
                result = matches[0].Groups[1].Value;
            }
            catch (ArgumentException)
            {
            }

            if (result == null)
            {
                return "default";
            }

            string body = result.Trim();
            return body;
        }

        public string ExtractCampaignContinuitySection(string storyText)
        {
            string extractContinuity = @"^##\s[Kk]ontynuacja[:|\s]*$(.+?)(\Z|^#{1,2}\s\w+)";
            string extractCampaign = @"###\s[Kk]ampanijna[:|\s]*$(.+?)(\Z|^#{1,3}\s\w+)";
            string extractLinkTarget = @"^\*\s\[.+?\]\((.+?)\)";

            string continuitySection = null;
            try
            {
                MatchCollection matches = Regex.Matches(storyText, extractContinuity, RegexOptions.Singleline | RegexOptions.Multiline);
                continuitySection = matches[0].Groups[1].Value;
            }
            catch (ArgumentException)
            {
            }

            if (continuitySection == null)
            {
                return "default";
            }

            string continuity = continuitySection.Trim();

            string campaignSection = null;
            try
            {
                MatchCollection matches = Regex.Matches(continuity, extractCampaign, RegexOptions.Singleline | RegexOptions.Multiline);
                campaignSection = matches[0].Groups[1].Value;
            }
            catch (ArgumentException)
            {
            }

            if (campaignSection == null)
            {
                return "default";
            }

            string campaign = campaignSection.Trim();

            string linkTarget = null;
            try
            {
                MatchCollection matches = Regex.Matches(campaign, extractLinkTarget, RegexOptions.Singleline | RegexOptions.Multiline);
                linkTarget = matches[0].Groups[1].Value;
            }
            catch (ArgumentException)
            {
            }

            if (linkTarget == null)
            {
                return "default";
            }

            return linkTarget.Trim();
        }

    }
}
