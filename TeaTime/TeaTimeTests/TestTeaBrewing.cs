﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TeaTime.Actions;
using TeaTime.Entities;

namespace TeaTimeTests
{
    public class TestTeaBrewing
    {
        [Test]
        public void TeaBrewedAccordingToRecipe()
        {
            // Given
            var inputs = (temp: 96, time: 300);
            TeaRecipe recipe = new TeaRecipe("Mięta", "napar", 96, 5, "smaczna");

            // Expected
            Tea expected = new Tea("Mięta", "perfect");

            // When
            Tea actual = new BrewTea().Execute(recipe, inputs);

            //Then
            Assert.AreEqual(expected, actual);

        }
    }
}
