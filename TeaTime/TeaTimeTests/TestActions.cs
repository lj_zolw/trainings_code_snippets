using NUnit.Framework;
using TeaTime.Actions;

namespace Tests
{
    public class TestActions
    {

        [Test]
        public void Action2WorksForSimpleCase()
        {
            // Given
            string textToSort = "Lapacho, napar, 96, 10, \nGunpowder Zielony, zielona, 70, 3, \nMi�ta, napar, 96, 5, ";

            // Expected
            string expected = "Lapacho, napar, 96, 10, \nMi�ta, napar, 96, 5, \nGunpowder Zielony, zielona, 70, 3, ";

            // When
            string actual = new SortText().Execute(textToSort);

            // Then
            Assert.True(expected == actual);
        }

        [Test]
        public void Action1WorksForSimpleCase()
        {
            // Given
            string textToReverse = "Ala\nma \nfajnego kota";

            // Expected
            string expected = "fajnego kota\nma \nAla";

            // When
            string actual = new ReverseText().Execute(textToReverse);

            // Then
            Assert.True(expected == actual);
        }

    }
}