﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using TeaTime.Entities;

namespace TeaTimeTests
{
    public class TestTeaRecipe
    {
        [Test]
        public void TeaCanBeMade()
        {
            // Given
            var record = "Sen o smoku, czarna, 96, 3, ostra: z pieprzem i imbirem";

            // Expected
            TeaRecipe expected = new TeaRecipe("Sen o smoku", "czarna", 96, 3, "ostra: z pieprzem i imbirem");

            // When
            TeaRecipe actual = TeaRecipeFactory.CreateSingle(record);

            // Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ManyTeasCanBeMade()
        {
            // Given
            var records = "Lapacho, napar, 96, 10, \nMięta, napar, 96, 5, ";

            // When
            List<TeaRecipe> actual = TeaRecipeFactory.CreateMany(records);

            // Then
            Assert.IsTrue(actual.Count == 2);
            Assert.IsTrue(actual[0].Name == "Lapacho");
        }

        [Test]
        public void ManyTeasCanBeMadeAndDamagedRecordsAreIgnored()
        {
            // Given
            var records = "Lapacho, napar, 96, 10, \nMięta, napar, 96, 5, \nsdlakdlakda;lkdad\nZwykła, czarna, 96, 3, ";

            // When
            List<TeaRecipe> actual = TeaRecipeFactory.CreateMany(records);

            // Then
            Assert.IsTrue(actual.Count == 3);
            Assert.IsTrue(actual[0].Name == "Lapacho");
        }

    }
}
