﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace TeaTime.Tools
{
    public class FileOps
    {
        public static string PathToDataFolder
        {
            get
            {
                var currentPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
                var dataPath = currentPath.Remove(currentPath.IndexOf("bin")) + "\\_io";
                return dataPath;
            }
        }

        internal static void SaveFileToOutputs(string filename, string content)
        {
            string filePath = PathToDataFolder + "\\" + filename;
            File.WriteAllText(filePath, content);
        }

        internal static string ReadTeaFile()
        {
            var filePaths = Directory.EnumerateFiles(PathToDataFolder);

            string filePath = SelectSingleFilepath(filePaths, "tea-data");
            return File.ReadAllText(filePath);
        }

        static string SelectSingleFilepath(IEnumerable<string> filePaths, string pathElement)
        {
            return filePaths.Where(path => path.Contains(pathElement)).First();
        }
    }
}

