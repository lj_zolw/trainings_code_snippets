﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TeaTime.Entities
{
    public class Tea
    {
        public Tea(string name, string quality)
        {
            this.Name = name;
            this.Quality = quality;
        }

        public string Name { get; set; }
        public string Quality { get; set; }

        public override string ToString() => $"{Name}: {Quality}";

        public override bool Equals(object obj) => this.ToString().Equals(obj.ToString());
        public override int GetHashCode() => HashCode.Combine(Name, Quality);

    }
}
