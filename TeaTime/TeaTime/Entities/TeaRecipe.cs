﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TeaTime.Entities
{
    public class TeaRecipe
    {
        public TeaRecipe(string name, string type, int temperature, int time, string special)
        {
            this.Name = name;
            this.Type = type;
            this.Temperature = temperature;
            this.Time = time;
            this.Special = special;
        }

        public string Name { get; set; }
        public string Type { get; set; }
        public int Temperature { get; set; }
        public int Time { get; set; }
        public string Special { get; set; }

        public override string ToString()
        {
            return $"{Name} | {Type} | {Temperature} | {Time} | {Special}";
        }

        public override bool Equals(object other)
        {
            if (other is TeaRecipe == false) return false;
            TeaRecipe otherTea = other as TeaRecipe;

            if (this.Name != otherTea.Name) return false;
            if (this.Type != otherTea.Type) return false;
            if (this.Temperature != otherTea.Temperature) return false;
            if (this.Time != otherTea.Time) return false;
            if (this.Special != otherTea.Special) return false;

            return true;
        }

        public override int GetHashCode() => HashCode.Combine(Name, Type, Temperature, Time, Special);

    }
}
