﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace TeaTime.Entities
{
    public static class TeaRecipeFactory
    {
        private static readonly string _teaRegex = @"^([\w\s]+),([\w\s]+),\s*(\d+),\s*(\d+),([\w\s:]+)$";

        public static List<TeaRecipe> CreateMany(string text)
        {
            Regex r = new Regex(_teaRegex, RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.Multiline);
            var records = r.Matches(text);

            List<TeaRecipe> recipes = new List<TeaRecipe>();
            foreach (Match found in records)
            {
                recipes.Add(
                    new TeaRecipe(name: found.Groups[1].Value.Trim(),
                        type: found.Groups[2].Value.Trim(),
                        temperature: int.Parse(found.Groups[3].Value),
                        time: int.Parse(found.Groups[4].Value),
                        special: found.Groups[5].Value.Trim())
                        );
            }

            return recipes;
        }

        public static TeaRecipe CreateSingle(string record)
        {
            Regex r = new Regex(_teaRegex);
            var found = r.Match(record);

            return new TeaRecipe(name: found.Groups[1].Value.Trim(), 
                type: found.Groups[2].Value.Trim(), 
                temperature: int.Parse(found.Groups[3].Value), 
                time: int.Parse(found.Groups[4].Value), 
                special: found.Groups[5].Value.Trim());
        }
    }
}
