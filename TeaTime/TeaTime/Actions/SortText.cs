﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace TeaTime.Actions
{
    public class SortText
    {
        public string Execute(string textToSort)
        {
            var split = textToSort.Split("\n");
            Array.Sort(split, new MySorter());
            var merged = String.Join("\n", split);
            return merged;
        }
    }


    public class MySorter : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            string pattern = @"^[\w\s]+,([\w\s]+),\s*\d+,\s*\d+,[\w\s:]+$";

            var recordX = new Regex(pattern).Match(x).Groups[1].Value.Trim();
            var recordY = new Regex(pattern).Match(y).Groups[1].Value.Trim();

            var result =  recordX.CompareTo(recordY);
            return result;
        }
    }

}
