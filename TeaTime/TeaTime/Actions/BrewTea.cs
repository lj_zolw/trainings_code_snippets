﻿using System;
using System.Collections.Generic;
using System.Text;
using TeaTime.Entities;

namespace TeaTime.Actions
{
    public class BrewTea
    {
        public Tea Execute(TeaRecipe recipe, (int temp, int time) inputs)
        {
            int actualRecipeTime = recipe.Time * 60;

            (int min, int max) = (recipe.Temperature * 9 / 10, recipe.Temperature * 11 / 10);
            var time = (min: actualRecipeTime * 9 / 10, max: actualRecipeTime * 11 / 10);

            string teaPerfection = "unknown";
            if (inputs.temp > min && inputs.temp < max && inputs.time > time.min && inputs.time < time.max) teaPerfection = "perfect";
            else if (inputs.temp > max || inputs.time > time.max) teaPerfection = "yucky";
            else if (inputs.temp < min || inputs.time < time.min) teaPerfection = "weak";
            else throw new ArgumentException("Wrong level of perfection while making tea");

            return new Tea(recipe.Name, teaPerfection);
        }
    }
}
