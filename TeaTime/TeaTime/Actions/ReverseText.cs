﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TeaTime.Actions
{
    public class ReverseText
    {
        public string Execute(string textToReverse)
        {
            var split = textToReverse.Split("\n");
            Array.Reverse(split);
            var merged = String.Join("\n", split);
            return merged;
        }
    }
}
