﻿using System;
using TeaTime.Tasks;

namespace TeaTime
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Which task do you want to have completed? 1-6.");
            var result = Console.ReadKey();

            if (result.KeyChar == '1') new Task01().Run();
            if (result.KeyChar == '2') new Task02().Run();
            if (result.KeyChar == '4') new Task04().Run();

        }
    }
}
