﻿using System;
using TeaTime.Actions;
using TeaTime.Tools;

namespace TeaTime.Tasks
{
    public class Task01
    {
        public void Run()
        {
            var readText = FileOps.ReadTeaFile();
            var reversed = new ReverseText().Execute(readText);
            FileOps.SaveFileToOutputs("result1.txt", reversed);
        }
    }
}