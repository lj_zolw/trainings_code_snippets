﻿using System;
using TeaTime.Actions;
using TeaTime.Tools;

namespace TeaTime.Tasks
{
    public class Task02
    {
        public void Run()
        {
            var readText = FileOps.ReadTeaFile();
            var reversed = new SortText().Execute(readText);
            FileOps.SaveFileToOutputs("result2.txt", reversed);
        }
    }
}