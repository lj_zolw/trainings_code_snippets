﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TeaTime.Actions;
using TeaTime.Entities;
using TeaTime.Tools;

namespace TeaTime.Tasks
{
    public class Task04
    {
        public void Run()
        {
            Console.WriteLine("Which tea do you want?");
            var teaType = Console.ReadLine();
            Console.WriteLine("Temperature?");
            int temperature = int.Parse(Console.ReadLine());
            Console.WriteLine("Time?");
            int time = int.Parse(Console.ReadLine());

            var readText = FileOps.ReadTeaFile();
            var recipes = TeaRecipeFactory.CreateMany(readText);
            var recipe = (from r in recipes where r.Name == teaType select r).First();

            var tea = new BrewTea().Execute(recipe, (temperature, time));

            var content = $"{tea.ToString()}";
            FileOps.SaveFileToOutputs("result4.txt", content);
        }
    }
}
