﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeMergingForFun
{
    public class MergeTrees
    {
        internal string Merge(string tree1, string tree2)
        {
            // First we split trees into lines
            List<string> tree1_lines = SplitTree(tree1);
            List<string> tree2_lines = SplitTree(tree2);

            // In each record, delete the stuff after the comma
            List<string> cleared_tree1_lines = DeleteStuffAfterComma(tree1_lines);
            List<string> cleared_tree2_lines = DeleteStuffAfterComma(tree2_lines);

            // For each tree, build a Tuple of: Path, Depth, Value
            List<Tuple<string, int, string>> tree1_records = LinesToRecords(cleared_tree1_lines);
            List<Tuple<string, int, string>> tree2_records = LinesToRecords(cleared_tree2_lines);

            // Having paths, merge the records
            List<Tuple<string, int, string>> summed_records = CreateTreeFromTwo(tree1_records, tree2_records);

            // And finally, return to string mode:
            string summed_tree = RecordsToSingleString(summed_records);
            return summed_tree;
        }

        private string RecordsToSingleString(List<Tuple<string, int, string>> summed_records)
        {
            // First pass - make a list of strings
            List<string> tree_lines = new List<string>();

            foreach(var record in summed_records)
            {
                tree_lines.Add(record.Item3);
            }

            // Second pass - join them

            string merged_string = string.Join(System.Environment.NewLine, tree_lines);
            return merged_string;
        }

        private List<Tuple<string, int, string>> CreateTreeFromTwo(List<Tuple<string, int, string>> tree1_records, List<Tuple<string, int, string>> tree2_records)
        {
            // First pass - merge them to common tree
            List<Tuple<string, int, string>> master_tree = new List<Tuple<string, int, string>>(tree1_records);

            foreach(var candidate in tree2_records)
            {
                // Candidate must belong SOMEWHERE. It will belong in the spot where its PATH will be the most similar.
                // Going forward with candidates we ensure that there will ALWAYS be a potential parent
                // Going backward with master_tree we ensure that there will ALWAYS be the most precise parent

                for (int i = master_tree.Count-1; i>=0; i--)
                {
                    var potential_parent = master_tree[i];
                    var potential_path_if_parent = potential_parent.Item1 + potential_parent.Item3.Trim();
                    if (potential_path_if_parent == candidate.Item1)
                    {
                        // We have a match
                        master_tree.Insert(i+1, candidate);
                        break;
                    }
                }
            }

            // Second pass - make them unique
            List<Tuple<string, int, string>> unique_master_tree = master_tree.Distinct().ToList();

            return unique_master_tree;
        }

        private List<Tuple<string, int, string>> LinesToRecords(List<string> cleared_tree_lines)
        {

            // First pass - create a (depth, name) Tuples for easier calculation.
            List<Tuple<int, string>> tree_depth_lines = new List<Tuple<int, string>>();

            foreach (var line in cleared_tree_lines)
            {
                int depth = line.Length - line.TrimStart(' ').Length;
                tree_depth_lines.Add(new Tuple<int, string>(depth, line));
            }

            // Second pass - create a skeleton of paths.
            List<Tuple<string, int, string>> dirty_tree_records = new List<Tuple<string, int, string>>();

            for (int i=0; i< tree_depth_lines.Count; i++)
            {
                var current_element = tree_depth_lines[i];
                string path = string.Empty;

                if(current_element.Item1 == 0)
                {
                    path = "ROOT|" + current_element.Item2.Trim();
                }
                else
                {
                    bool found_parent = false;
                    int j = i -1;
                    while(found_parent == false)
                    {
                        var potential_parent = tree_depth_lines[j];
                        if (potential_parent.Item1 < current_element.Item1)
                        {
                            // We have a parent and we need to use a WORKING path
                            // due to parity of those collections
                            path = dirty_tree_records[j].Item1 + current_element.Item2.Trim();
                            found_parent = true;
                        }
                        else
                        {
                            // We search for a new parent
                            j = j - 1;
                        }
                    }
                }

                dirty_tree_records.Add(new Tuple<string, int, string>(path, current_element.Item1, current_element.Item2));
            }

            // Third pass - eliminate the self-referencing record from the paths.
            List<Tuple<string, int, string>> final_tree_records = new List<Tuple<string, int, string>>();

            foreach (var record in dirty_tree_records)
            {
                string cleaned_path = record.Item1.Substring(0, record.Item1.IndexOf(record.Item3.Trim()));
                final_tree_records.Add(new Tuple<string, int, string>(cleaned_path, record.Item2, record.Item3));
            }

            return final_tree_records;
        }


        private List<string> DeleteStuffAfterComma(List<string> tree_lines)
        {
            List<string> cleared_tree_lines = new List<string>();
            foreach(var line in tree_lines)
            {
                string cleared_line = line;
                if (cleared_line.Contains(","))
                {
                    cleared_line = line.Substring(0, line.IndexOf(","));
                }

                cleared_tree_lines.Add(cleared_line);
            }

            return cleared_tree_lines;
        }


        private List<string> SplitTree(string tree)
        {
            var split = tree.Split(new[] { System.Environment.NewLine }, StringSplitOptions.None);
            List<string> list = new List<string>(split);
            return list;
        }
    }
}
