﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeMergingForFun
{
    class Program
    {
        static void Main(string[] args)
        {
            string tree1 = @"1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, centralne dowodzenie Pauliny gdzie Kaja sprząta ;-). I ogólnie dużo tam się zaklęć ostatnio uziemiało.
                1. Senesgrad
                    1. Dworcowo
                        1. Kolejowa wieża ciśnień, którą chciał wykorzystać Prosperjusz, którą badał Grazoniusz i którą z oddali sensorami obudowuje Kaja.
                1. Trzeszcz
                    1. Elektronik Suwak, sklep elektroczny ze znajomym elektronikiem Pauliny (Karolem Komnatem)
                1. Praszyna
                    1. Redakcja Głosu Praszyny, magazyn plotkarski w którym czasem dochodzi do (zmyślonego zwykle) łamania Maskarady; Paulina musi się tym zajmować...
                    1. Ptasiówka, gdzie Paulina umiesciła Prosperjusza i jego biznes. 
            1. Powiat Głuszców
                1. Upiorzec
                    1. Eliksir Aerinus, gdzie Paulina rozmawiając z Apoloniuszem załatwiła Hektorowi pracę.";

            string tree2 = @"1. Świat
    1. Primus
        1. Mazowsze
            1. Powiat Pustulski
                1. Krukowo Czarne
                    1. Gabinet Pauliny, miejsce wielkiej dramy między Hektorem, Dianą i Grazoniuszem
                        1. Ogródek, zniszczony doszczętnie przez katastrofę magiczną.
                1. Senesgrad
                    1. Dworcowo
                        1. Kolejowa wieża ciśnień, gdzie - zdaniem Grazoniusza - coś nowego i dziwnego zaczyna się dziać
                1. Miauszczur
                    1. Wchodzix";

            string tree3 = @"1. Świat
    1. Primus
        1. Śląsk
            1. Powiat Kopaliński 
                1. Kopalin
                    1. Obrzeża
                        1. Rezydencja Blakenbauerów, dokąd wrócił Hektor, gdzie jest Judyta i ogólnie miejsce które można nazwać domem.
                    1. Las Stu Kotów
                        1. Osiedle Leśne
                            1. Chatka Zegarowa, gdzie doszło do wielkiej tragedii - Alegretta porywała tam lekarzy";

            string result = new MergeTrees().Merge(tree1, tree2);
            result = new MergeTrees().Merge(result, tree3);

            Console.Write(result);
            Console.ReadKey();
        }
    }
}
